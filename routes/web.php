<?php

use App\Http\Controllers\VehiclesController;
use App\Http\Controllers\VehiclesUsersController;
use App\Http\Controllers\HistoricController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//  index
Route::get('/', [VehiclesController::class, 'index'])->name('vehicles.index');


//  users routes
Route::get('/index_users', [VehiclesUsersController::class, 'index'])->name('users.index');
Route::get('/create_user', [VehiclesUsersController::class, 'create'])->name('user.create');
Route::post('/store_user', [VehiclesUsersController::class, 'store'])->name('user.store');
Route::get('/edit_user/{id}', [VehiclesUsersController::class, 'edit'])->name('user.edit');
Route::put('/update_user/{id}', [VehiclesUsersController::class, 'update'])->name('user.update');
Route::delete('/destroy_user/{id}', [VehiclesUsersController::class, 'destroy'])->name('user.destroy');

//  vehicles routes
Route::get('/create_vehicle', [VehiclesController::class, 'create'])->name('vehicle.create');
Route::post('/store_vehicle', [VehiclesController::class, 'store'])->name('vehicle.store');
Route::get('/edit_vehicle/{id}', [VehiclesController::class, 'edit'])->name('vehicle.edit');
Route::put('/update_vehicle/{id}', [VehiclesController::class, 'update'])->name('vehicle.update');
Route::delete('/destroy_vehicle/{id}', [VehiclesController::class, 'destroy'])->name('vehicle.destroy');

//  historic route

Route::get('/index_historic', [HistoricController::class, 'index'])->name('historic.index');


