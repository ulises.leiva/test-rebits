@extends('layout/template')

@section('contenido')

    <div class="card">
    <h5 class="card-header">Actualizar usuario</h5>
    <div class="card-body">
        <p class="card-text">
            <form action="{{ route('user.update', $vehicle_user->id)}}" method="POST">
                @csrf
                @method("PUT")
                <label for=""> Nombre </label>
                <input type="text" name="name" class="form-control" required value="{{$vehicle_user->name}}">
                <label for=""> Apellidos </label>
                <input type="text" name="surnames" class="form-control" required value="{{$vehicle_user->surnames}}">
                <label for=""> Correo </label>
                <input type="text" name="email" class="form-control" required value="{{$vehicle_user->email}}">
                <br>
                <a href="{{ route("users.index") }}" class="btn btn-secondary" > Volver </a>
                <button class= "btn btn-warning">Actualizar</button>
            </form>
        </p>
    </div>
    </div>
@endsection