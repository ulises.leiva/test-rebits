@extends('layout/template')

@section('contenido')
    <div class="card">
    <div class="card-header">
        <ul class="nav nav-pills card-header-pills">
        <li class="nav-item">
            <a href="{{ route("vehicles.index") }}" class="nav-link" href="#">Vehiculos</a>
        </li>
        <li class="nav-item">
            <a href="{{ route("users.index") }}" class="nav-link active" href="#">Usuarios</a>
        </li>
        <li class="nav-item">
            <a href="{{ route("historic.index") }}" class="nav-link" href="#">Histórico</a>
        </li>
        </ul>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        {{ $message }}
                    </div>
                @endif
            </div>
        </div>
        <div style="text-aling: center">
            <h5 class="card-title text-center">Lista de personas.</h5>
        </div>
        <p>
            <a href="{{ route("user.create") }}" class="btn btn-primary"> <i class="fas fa-plus fa-xs"></i> <i class="fas fa-user fa-lg"></i>  Agregar usuario</a>
        </p>
        <table class="table table-bordered table-striped">
        <thead>
            <tr>
            <th scope="col">Id</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellidos</th>
            <th scope="col">Correo</th>
            <th scope="col"></th>
            <th scope="col"></th>
            </tr>
        </thead>
        @foreach ($vehicles_users as $vehicle_user)
            <tbody>
                <tr>
                <th scope="row">{{ $vehicle_user->id }}</th>
                <td>{{ $vehicle_user->name }}</td>
                <td>{{ $vehicle_user->surnames }}</td>
                <td>{{ $vehicle_user->email }}</td>
                <td>
                    <form action="{{ route('user.edit', $vehicle_user->id)}}" method="GET">
                        <button class="btn btn-secondary"> <i class="fas fa-edit"></i> <i class="fas fa-user fa-lg"></i>  Editar usuario </button>
                    </form>
                </td>
                <td> 
                    <form action="{{ route('user.destroy', $vehicle_user->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger"> <i class="fas fa-trash-alt"></i> <i class="fas fa-user fa-lg"></i>  Eliminar usuario </button> 
                    </form>
                </td>
                </tr>
            </tbody>
        @endforeach
        </table>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                {{ $vehicles_users->links()}}
            </div>
        </div>
    </div>
    </div>
@endsection
