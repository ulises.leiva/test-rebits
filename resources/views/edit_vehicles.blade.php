@extends('layout/template')

@section('contenido')

    <div class="card">
    <h5 class="card-header">Actualizar Vehículo</h5>
    <div class="card-body">
        <p class="card-text">
             <form action="{{ route('vehicle.update', $vehicle->id)}}" method="POST">
                @csrf
                @method("PUT")
                <label for=""> Marca </label>
                <input type="text" name="brand" class="form-control" required value="{{$vehicle->brand}}">
                <label for=""> Modelo </label>
                <input type="text" name="model" class="form-control" required value="{{$vehicle->model}}">
                <label for=""> Año </label>
                <input type="integer" name="year" class="form-control" required value="{{$vehicle->year}}">
                <div class="form-group">
                    <label for="owner">Dueño:</label>
                    <select name="owner" id="owner">
                    @foreach ($vehicles_users as $vehicle_user)
                        <option value="{{ $vehicle_user->id }}" {{$vehicle_user->id == $vehicle->user_id ? 'selected' : ''}}>
                            {{ $vehicle_user->name }} {{ $vehicle_user->surnames }}
                        </option>
                    @endforeach
                    </select>
                </div>
                <label for=""> Precio </label>
                <input type="integer" name="price" class="form-control" required value="{{$vehicle->price}}">
                <br>
                <a href="{{ route("vehicles.index") }}" class="btn btn-secondary" > Volver </a>
                <button class= "btn btn-warning">Actualizar</button>
            </form>

        </p>
    </div>
    </div>
@endsection