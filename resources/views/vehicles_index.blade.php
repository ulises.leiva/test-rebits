@extends('layout/template')

@section('contenido')
    <div class="card">
    <div class="card-header">
        <ul class="nav nav-pills card-header-pills">
        <li class="nav-item">
            <a href="{{ route("vehicles.index") }}" class="nav-link active" href="#">Vehiculos</a>
        </li>
        <li class="nav-item">
            <a href="{{ route("users.index") }}" class="nav-link" href="#">Usuarios</a>
        </li>
        <li class="nav-item">
            <a href="{{ route("historic.index") }}" class="nav-link" href="#">Histórico</a>
        </li>
        </ul>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        {{ $message }}
                    </div>
                @elseif ($message = Session::get('failure'))
                    <div class="alert alert-warning" role="alert">
                        {{ $message }}
                    </div>
                @endif
            </div>
        </div>
        <div style="text-aling: center">
        <h5 class="card-title text-center">Lista de vehículos.</h5>
        </div>
        <p>
            <a href="{{ route("vehicle.create") }}" class="btn btn-primary"> <i class="fas fa-plus fa-xs"></i> <i class="fas fa-car fa-lg"></i> Agregar vehículo</a>
        </p>
        <table class="table table-bordered table-striped">
        <thead>
            <tr>
            <th scope="col">Id</th>
            <th scope="col">Marca</th>
            <th scope="col">Modelo</th>
            <th scope="col">Año</th>
            <th scope="col">Dueño</th>
            <th scope="col">Precio</th>
            <th scope="col"></th>
            <th scope="col"></th>
            </tr>
        </thead>
        @foreach ($vehicles as $vehicle)
            <tbody>
                <tr>
                <th scope="row">{{ $vehicle->id }}</th>
                <td>{{ $vehicle->brand }}</td>
                <td>{{ $vehicle->model }}</td>
                <td>{{ $vehicle->year }}</td>
                <td> {{ $full_name[$vehicle->user_id] }}</td>
                <td>{{ $vehicle->price }}</td>
                
                <td> 
                    <form action="{{ route('vehicle.edit', $vehicle->id)}}" method="GET">
                        <button class="btn btn-secondary"> <i class="fas fa-edit"></i> <i class="fas fa-car fa-lg"></i> Editar vehículo </button>
                    </form>
                </td>
                <td> 
                    <form action="{{ route('vehicle.destroy', $vehicle->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger"> <i class="fas fa-trash-alt"></i> <i class="fas fa-car fa-lg"></i> Eliminar vehículo </button>
                    </form>
                </td>
                </tr>
            </tbody>
        @endforeach
        </table>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                {{ $vehicles->links()}}
            </div>
        </div>
        
    </div>
    </div>
@endsection
