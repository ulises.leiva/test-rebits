@extends('layout/template')

@section('contenido')

    <div class="card">
    <h5 class="card-header">Agregar usuario</h5>
    <div class="card-body">
        <p class="card-text">
            <form action="{{ route('user.store') }}" method="POST">
                @csrf
                <label for=""> Nombre </label>
                <input type="text" name="name" class="form-control" required>
                <label for=""> Apellidos </label>
                <input type="text" name="surnames" class="form-control" required>
                <label for=""> Correo </label>
                <input type="text" name="email" class="form-control" required>
                <br>
                <a href="{{ route('users.index') }}" class="btn btn-secondary" > Volver </a>
                <button class= "btn btn-primary">Agregar</button>
            </form>

        </p>
    </div>
    </div>
@endsection