@extends('layout/template')

@section('contenido')
    <div class="card">
    <div class="card-header">
        <ul class="nav nav-pills card-header-pills">
        <li class="nav-item">
            <a href="{{ route("vehicles.index") }}" class="nav-link" href="#">Vehiculos</a>
        </li>
        <li class="nav-item">
            <a href="{{ route("users.index") }}" class="nav-link" href="#">Usuarios</a>
        </li>
        <li class="nav-item">
            <a href="{{ route("historic.index") }}" class="nav-link active" href="#">Histórico</a>
        </li>
        </ul>
    </div>
    <div class="card-body">
        <div style="text-aling: center">
        <h5 class="card-title text-center">Lista de vehículos.</h5>
        </div>
        <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Marca</th>
                <th scope="col">Modelo</th>
                <th scope="col">Año</th>
                <th scope="col">Dueño</th>
                <th scope="col">Precio</th>
                <th scope="col">Operación</th>
            </tr>
        </thead>
        @foreach ($historic as $register)
            <tbody>
                <tr class="{{ $register->colorClass }}">
                    <th scope="row">{{ $register->id }}</th>
                    <td>{{ $register->brand }}</td>
                    <td>{{ $register->model }}</td>
                    <td>{{ $register->year }}</td>
                    <td> {{ $register->owner }}</td>
                    <td>{{ $register->price }}</td>
                    @if ($register->operation === 'C')
                        <td>Creado</td>
                    @elseif ($register->operation === 'E')
                        <td>Editado</td>
                    @else
                        <td>Eliminado</td>
                    @endif
                </tr>
            </tbody>
        @endforeach
        </table>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                {{ $historic->links()}}
            </div>
        </div>
        
    </div>
    </div>
@endsection