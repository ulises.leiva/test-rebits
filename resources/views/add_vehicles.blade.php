@extends('layout/template')

@section('contenido')

    <div class="card">
    <h5 class="card-header">Agregar vehículo</h5>
    <div class="card-body">
        <p class="card-text">
            <form action="{{ route('vehicle.store') }}" method="POST">
                @csrf
                <label for=""> Marca </label>
                <input type="text" name="brand" class="form-control" required>
                <label for=""> Modelo </label>
                <input type="text" name="model" class="form-control" required>
                <label for=""> Año </label>
                <input type="integer" name="year" class="form-control" required>
                <div class="form-group">
                    <label for="owner">Dueño:</label>
                    <select name="owner" id="owner">
                    @foreach ($vehicles_users as $vehicles_user)
                        <option value="{{ $vehicles_user->id }}">
                            {{ $vehicles_user->name }} {{ $vehicles_user->surnames }}
                        </option>
                    @endforeach
                    </select>
                </div>
                <label for=""> Precio </label>
                <input type="integer" name="price" class="form-control" required>
                <br>
                <a href="{{ route("vehicles.index") }}" class="btn btn-secondary" > Volver </a>
                <button class= "btn btn-primary">Agregar</button>
            </form>

        </p>
    </div>
    </div>
@endsection