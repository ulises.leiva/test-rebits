<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        Schema::create('vehicles_users', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->id();
            $table->string('name');
            $table->string('surnames');
            $table->string('email');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('vehicles', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->id();
            $table->string('brand');
            $table->string('model');
            $table->integer('year');
            $table->bigInteger('user_id')->unsigned();
            $table->integer('price');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('vehicles_users');
        });

        Schema::create('historic', function (Blueprint $table) {
            $table->id();
            $table->string('brand');
            $table->string('model');
            $table->integer('year');
            $table->string('owner');
            $table->integer('price');
            $table->char('operation', 1); //  C = create; E = edit; D = delete
            $table->timestamps(); 
        });
    }
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('historic');
        Schema::dropIfExists('vehicles');
        Schema::dropIfExists('vehicles_users');
        
        
    }
};
