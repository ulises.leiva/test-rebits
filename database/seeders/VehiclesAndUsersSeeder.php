<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\VehiclesUsers;
use App\Models\Vehicles;
use App\Models\Historic;

class VehiclesAndUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $vehicle_user_1 = new VehiclesUsers();
        $vehicle_user_1->name = "Matías";
        $vehicle_user_1->surnames = "Espinoza Antonetti";
        $vehicle_user_1->email = "matias@correo.com";
        $vehicle_user_1->save();

        $vehicle_user_2 = new VehiclesUsers();
        $vehicle_user_2->name = "Camila";
        $vehicle_user_2->surnames = "García Sepulveda";
        $vehicle_user_2->email = "camila@correo.com";
        $vehicle_user_2->save();

        $vehicle_1 = new Vehicles();
        $vehicle_1->brand = "Toyota";
        $vehicle_1->model = "Corola";
        $vehicle_1->year = 2023;
        $vehicle_1->user_id = $vehicle_user_1->id;
        $vehicle_1->price = 25000000;
        $vehicle_1->save();

        $vehicle_2 = new Vehicles();
        $vehicle_2->brand = "BMW";
        $vehicle_2->model = "Serie 3";
        $vehicle_2->year = 2023;
        $vehicle_2->user_id = $vehicle_user_2->id;
        $vehicle_2->price = 41000;
        $vehicle_2->save();

        $historical_1 = new Historic();
        $historical_1->brand = $vehicle_1->brand;
        $historical_1->model = $vehicle_1->model;
        $historical_1->year = $vehicle_1->year;
        $historical_1->owner = $vehicle_user_1->name . ' ' . $vehicle_user_1->surnames;
        $historical_1->price = $vehicle_1->price;
        $historical_1->operation = 'C';
        $historical_1->save();

        $historical_2 = new Historic();
        $historical_2->brand = $vehicle_2->brand;
        $historical_2->model = $vehicle_2->model;
        $historical_2->year = $vehicle_2->year;
        $historical_2->owner = $vehicle_user_2->name . ' ' . $vehicle_user_2->surnames;
        $historical_2->price = $vehicle_2->price;
        $historical_2->operation = 'C';
        $historical_2->save();
    }
}
