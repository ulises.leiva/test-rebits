<?php

namespace App\Http\Controllers;

use App\Models\VehiclesUsers;
use App\Models\Historic;
use App\Models\Vehicles;
use Illuminate\Http\Request;

class VehiclesUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $vehicles_users = VehiclesUsers::paginate(10);
        return view('index_users', compact('vehicles_users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('add_user');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $vehicle_user = new VehiclesUsers();
        $vehicle_user->name = $request->post('name');
        $vehicle_user->surnames = $request->post('surnames');
        $vehicle_user->email = $request->post('email');
        $vehicle_user->save();

        return redirect()->route("users.index")->with("success", "¡Usuario añadido con exito!");
    }

    /**
     * Display the specified resource.
     */
    public function show(VehiclesUsers $usuarios)
    {

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $vehicle_user = VehiclesUsers::find($id);
        return view('edit_user', compact('vehicle_user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $vehicle_user = VehiclesUsers::find($id);
        $vehicle_user->name = $request->post('name');
        $vehicle_user->surnames = $request->post('surnames');
        $vehicle_user->email = $request->post('email');
        $vehicle_user->save();

        return redirect()->route("users.index")->with("success", "¡Usuario añadido con exito!");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $vehicle_user = VehiclesUsers::find($id);
 
        $vehicles_to_delete = Vehicles::where('user_id', $vehicle_user->id)->get();
    
        foreach ($vehicles_to_delete as $vehicle) {
            $vehicle->delete();
            $historic = new Historic();
            $historic->brand = $vehicle->brand;
            $historic->model = $vehicle->model;
            $historic->year = $vehicle->year;
            $historic->owner = $vehicle_user->name . ' ' . $vehicle_user->usernames;
            $historic->price = $vehicle->price;
            $historic->operation = 'D';
            $historic->save();
        }
    
        $vehicle_user->delete();

        return redirect()->route("users.index")->with("success", "¡Usuario eliminado con exito!");
    }
}
