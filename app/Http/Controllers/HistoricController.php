<?php

namespace App\Http\Controllers;

use App\Models\Vehicles;
use App\Models\VehiclesUsers;
use App\Models\Historic;
use Illuminate\Http\Request;

class HistoricController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $historic = Historic::paginate(10);

        foreach ($historic as $register) {
            switch ($register->operation) {
                case 'C':
                    $register->colorClass = 'table-success';
                    break;
                case 'D':
                    $register->colorClass = 'table-danger';
                    break;
                case 'E':
                    $register->colorClass = 'table-warning';
                    break;
                default:
                    $register->colorClass = 'table-success';
                    break;
            }
        }
        return view('historic_index', compact('historic'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Historic $historic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Historic $historic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Historic $historic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Historic $historic)
    {
        //
    }
}
