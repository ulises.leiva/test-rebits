<?php

namespace App\Http\Controllers;

use App\Models\Vehicles;
use App\Models\VehiclesUsers;
use App\Models\Historic;
use Illuminate\Http\Request;

class VehiclesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $vehicles = Vehicles::paginate(10);
        $vehicles_users = VehiclesUsers::select('name', 'surnames', 'id')->get();
        
        $full_name = [];
        foreach ($vehicles_users as $user)  {
            $full_name[$user->id] = $user->name . ' ' . $user->surnames;
        }

        return view('vehicles_index', compact('vehicles', 'full_name'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $vehicles_users = VehiclesUsers::select('name', 'surnames', 'id')->get();
        if ($vehicles_users->isEmpty()){
            return redirect()->route("vehicles.index")->with("failure", "¡No existen usuarios de vehículos registrados!");
        }
        return view('add_vehicles', compact('vehicles_users'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $vehicle = new Vehicles();
        $vehicle->brand = $request->post('brand');
        $vehicle->model = $request->post('model');
        $vehicle->year = $request->post('year');
        $vehicle->user_id = $request->post('owner');
        $vehicle->price = $request->post('price');
        $vehicle->save();

        $vehicles_users = VehiclesUsers::select('name', 'surnames', 'id')->get();
        
        $full_name = [];
        foreach ($vehicles_users as $user)  {
            $full_name[$user->id] = $user->name . ' ' . $user->surnames;
        }

        

        $historic = new Historic();
        $historic->brand = $request->post('brand');
        $historic->model = $request->post('model');
        $historic->year = $request->post('year');
        $historic->owner = $full_name[$request->post('owner')];
        $historic->price = $request->post('price');
        $historic->operation = 'C';
        $historic->save();

        return redirect()->route("vehicles.index")->with("success", "¡Vehículo añadido con exito!");
    }

    /**
     * Display the specified resource.
     */
    public function show(Vehicles $vehiculos)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $vehicle = Vehicles::find($id);
        $vehicles_users = VehiclesUsers::select('name', 'surnames', 'id')->get();
        return view('edit_vehicles', compact('vehicle', 'vehicles_users'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $vehicle = Vehicles::find($id);
        $vehicle->brand = $request->post('brand');
        $vehicle->model = $request->post('model');
        $vehicle->year = $request->post('year');
        $vehicle->user_id = $request->post('owner');
        $vehicle->price = $request->post('price');
        $vehicle->save();

        $vehicles_users = VehiclesUsers::select('name', 'surnames', 'id')->get();
        
        $full_name = [];
        foreach ($vehicles_users as $user)  {
            $full_name[$user->id] = $user->name . ' ' . $user->surnames;
        }

        $historic = new Historic();
        $historic->brand = $request->post('brand');
        $historic->model = $request->post('model');
        $historic->year = $request->post('year');
        $historic->owner = $full_name[$request->post('owner')];
        $historic->price = $request->post('price');
        $historic->operation = 'E';
        $historic->save();

        return redirect()->route("vehicles.index")->with("success", "¡Vehículo actualizado con exito!");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        
        $vehicle = Vehicles::find($id);
        $vehicles_users = VehiclesUsers::select('name', 'surnames', 'id')->get();
        
        $full_name = [];
        foreach ($vehicles_users as $user)  {
            $full_name[$user->id] = $user->name . ' ' . $user->surnames;
        }

        $historic = new Historic();
        $historic->brand = $vehicle->brand;
        $historic->model = $vehicle->model;
        $historic->year = $vehicle->year;
        $historic->owner = $full_name[$vehicle->user_id];
        $historic->price = $vehicle->price;
        $historic->operation = 'D';
        $historic->save();
        
        $vehicle->delete();

        

        return redirect()->route("vehicles.index")->with("success", "¡Vehículo eliminado con exito!");
        
    }
}
