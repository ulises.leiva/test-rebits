<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\VehiclesUsers;


class TestEditVehicleUser extends TestCase
{
    public function testEditMethod()
    {
        $vehicleUser = VehiclesUsers::factory()->create();
        $response = $this->get("/vehicles-users/edit/{$vehicleUser->id}");
        $response->assertStatus(200);
        $response->assertViewIs('edit_user');
        $response->assertViewHas('vehicle_user', $vehicleUser);
    }
}
