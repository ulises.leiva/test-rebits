# CRUD para prueba de ReBits.

## Comandos utilizados para la instalación.

En primer lugar se instalan las dependencias necesarias utilizando:

```
composer install
```

se crea una llave aleatoria utilizada para temas de seguridad, cifrado y descifrado de datos:

```
php artisan key:generate
```

es importante crear la base de datos y configurarla dentro del archivo .env. Habiendo hecho esto se inicializan las tablas de la base de datos con:

```
php artisan migrate
```

Opcionalmente, si se desean utilizar las entradas de prueba en la base de datos ejecutar el comando:

```
php artisan  db:seed --class=VehiclesAndUsersSeeder
```

Finalmente, se inicia el servidor:

```
php artisan serve
```



